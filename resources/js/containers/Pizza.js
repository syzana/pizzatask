import React from 'react';
import axios from '../util/axios-config';
import PizzaList from "../components/PizzaList";
import {Col, Layout, Row, Menu, Breadcrumb, Modal, notification} from "antd";
import Login from "../components/login/Login";
import CookieUtil, {JWT_TOKEN} from "../util/CookieUtil";
import OrderList from "../components/order/OrderList";

const {Header, Content, Footer} = Layout;

class Pizza extends React.Component {

    constructor() {
        super();
        this.state = {
            pizzas: [],
            loginModalVisible: false,
            isLoggedIn: false,
            showOrders: false,
            showPizzas: true
        }
    }

    showLoginModal = () => this.setState({loginModalVisible: true});
    hideLoginModal = () => this.setState({loginModalVisible: false});


    async componentDidMount() {
        this.initPizzas();
    }


    async initPizzas() {
        const {data: pizzas} = await axios.get('/pizza');
        this.setState({pizzas: pizzas});
    }

    renderLoginModal = () => {
        return (
            <Login
                visible={this.state.loginModalVisible}
                setLoggedIn={() => this.setState({isLoggedIn: true})}
                hideModal={this.hideLoginModal}
                showModal={this.showLoginModal}
            />
        )
    }

    handleLogout = async () => {
        const jwtToken = CookieUtil.getCookie(JWT_TOKEN);
        const reqConfig = {
            headers:{
                'Authorization': `Bearer ${jwtToken}`
            }
        };
        const response = await axios.post('/logout',{},reqConfig);
        CookieUtil.deleteCookie(JWT_TOKEN);
        const {data: {message}} = response;
        notification.success({
            message: 'Success',
            description: message,
            placement: 'topRight'
        });
        this.setState({isLoggedIn: false});
    }


    showOrders = () => this.setState({showOrders: true, showPizzas: false});
    showPizzas = () => this.setState({showPizzas: true, showOrders: false});

    renderMenusBasedOnAuthentication = () => {
        const menus = [];
        if (this.state.isLoggedIn){
            menus.push(<Menu.Item style={{float: 'left'}} key="1" onClick={this.showOrders}>Orders</Menu.Item>);
            menus.push(<Menu.Item style={{float: 'right'}} key="3" onClick={this.handleLogout}>Logout</Menu.Item>);
        }else {
            menus.push(<Menu.Item style={{float: 'left'}} key="0" onClick={this.showPizzas}>Pizzas</Menu.Item>);
            menus.push(<Menu.Item onClick={this.showLoginModal} style={{float: 'right'}} key="2">Login</Menu.Item>);
        }
        return menus;
    }


    render() {
        let content =  [];
        if (this.state.showOrders){
            content.push(<OrderList/>);
        }
        if (this.state.showPizzas){
            content.push(<PizzaList pizzas={this.state.pizzas}/>)
        }

        return (
            <Layout className="layout">
                <Header>
                    <div className="logo"/>
                    <Menu
                        theme="dark"
                        mode="horizontal"
                        defaultSelectedKeys={['0']}
                        style={{lineHeight: '64px'}}
                    >
                        {this.renderMenusBasedOnAuthentication()}
                    </Menu>
                </Header>
                <Content style={{padding: '0 50px', height: '100vh'}}>
                    <Row>
                        <Col span={24}> <br/> </Col>
                        <Col span={24}>
                            <h1 style={{textAlign: 'center'}}>Order your pizza</h1>
                        </Col>
                        <Col span={24}>
                            {content}
                        </Col>
                    </Row>
                </Content>
                {this.renderLoginModal()}
            </Layout>
        )
    }
}

export default Pizza;