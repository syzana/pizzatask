import React from 'react';
import {List} from "antd";
import PizzaItem from "./pizza-item/PizzaItem";

const PizzaList = ({pizzas}) => {

    return (<List
        itemLayout="horizontal"
        dataSource={pizzas}
        renderItem={pizza => (
            <List.Item>
                <PizzaItem
                    pizza={pizza}/>
            </List.Item>
        )}
    />);
}

export default PizzaList;