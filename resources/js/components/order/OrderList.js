import React, {Component} from 'react';
import axios from '../../util/axios-config';
import CookieUtil from "../../util/CookieUtil";

class OrderList extends Component {
    state = {
        orders: []
    };

    async componentDidMount() {
        this.initOrders();
    }

    initOrders = async () => {
       const response = await axios.get('/order', CookieUtil.getAuthorizationReqConfig())
       const {data: orders} = response;
       this.setState({orders})
    }

    render() {
        return (
            <div>

            </div>
        );
    }
}

export default OrderList;