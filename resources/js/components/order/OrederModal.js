import React, {Component} from 'react'
import {Col, Form, Icon, Input, InputNumber, Modal, notification, Row} from "antd";
import axios from '../../util/axios-config'

const {TextArea} = Input;

class OrderForm extends Component {


    constructor(props) {
        super(props);
        const {closeModal} = props;
        this.closeModal = closeModal;
    }

    onSubmitFormHandler = (e) => {
        e.preventDefault();
        const {form, pizzaId} = this.props;
        form.validateFieldsAndScroll(async (err, values) => {
            if (!err) {
                const request = {...values};
                request.pizza_id = pizzaId;
                const response = await axios.post('/order', request);
                this.handleOrderResponse(response);
                this.closeModal();
            }

        })
    };

    handleOrderResponse = (response) => {
        if (response.data) {
            const {data: {message}} = response;
            notification.success({
                message: 'Success',
                description: message,
                placement: 'topRight'
            })
        }
    }


    render() {
        const {getFieldDecorator} = this.props.form;
        const {isModalVisible, pizzaName} = this.props;
        return (
            <Modal
                title={`Order now the "${pizzaName}" pizza`}
                visible={isModalVisible}
                onCancel={this.closeModal}
                onOk={this.onSubmitFormHandler}
            >
                <Form>
                    <Row gutter={12}>
                        <Col span={24}>
                            <Form.Item label="Full Name">
                                {getFieldDecorator('delivery_name', {
                                    rules: [{required: true, message: 'Please input your full name!'}],
                                })(
                                    <Input
                                        prefix={<Icon type="user" style={{color: 'rgba(0,0,0,.25)'}}/>}
                                    />,
                                )}
                            </Form.Item>
                        </Col>
                        <Col span={8}>
                            <Form.Item label="Street">
                                {getFieldDecorator('street', {
                                    rules: [{required: true, message: 'Please input your street!'}],
                                })(
                                    <Input/>,
                                )}
                            </Form.Item>
                        </Col>
                        <Col span={8}>
                            <Form.Item label="Plz">
                                {getFieldDecorator('plz', {
                                    rules: [{required: true, message: 'Please input your plz!'}],
                                })(
                                    <Input/>,
                                )}
                            </Form.Item>
                        </Col>
                        <Col span={8}>
                            <Form.Item label="City">
                                {getFieldDecorator('city', {
                                    rules: [{required: true, message: 'Please input your city!'}],
                                })(
                                    <Input/>,
                                )}
                            </Form.Item>
                        </Col>
                        <Col span={24}>
                            <Form.Item label="Nr of Pizzas">
                                {getFieldDecorator('nr_of_pizzas', {
                                    rules: [{required: true, message: 'Please input your nr. of pizzas!'}],
                                })(
                                    <InputNumber style={{width: '100%'}} min={1}/>,
                                )}
                            </Form.Item>
                        </Col>
                        <Col span={24}>
                            <Form.Item label="Description">
                                {getFieldDecorator('description', {})(
                                    <TextArea rows={4}/>,
                                )}
                            </Form.Item>
                        </Col>

                    </Row>
                </Form>
            </Modal>
        );

    }
}

export default Form.create()(OrderForm);