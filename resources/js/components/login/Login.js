import React from 'react';
import {Form, Icon, Input, Modal, notification} from "antd";
import axios from '../../util/axios-config'
import CookieUtil, {JWT_TOKEN} from "../../util/CookieUtil";

class Login extends React.Component{

    constructor(props){
        super(props);
        const {hideModal, setLoggedIn} = props;
        this.hideModal = hideModal;
        this.setLoggedIn = setLoggedIn;
    }

    onLoginSubmitHandler = () => {
        const {form: {validateFieldsAndScroll}} = this.props;
        validateFieldsAndScroll(async (err, values) => {
            if (!err){
                const response = await axios.post('/login', values);
                if (response.data){
                    const {data: {token: jwtToken}} = response;
                    CookieUtil.addOrReplaceCookie(JWT_TOKEN, jwtToken);
                    notification.success({
                        message: 'Success',
                        description: 'Login was successful',
                        placement: 'topRight'
                    });
                    this.hideModal();
                    this.setLoggedIn();
                }
            }
        })
    }

    render() {

        const { form:{ getFieldDecorator }, visible, hideModal } = this.props;
        return (
            <Modal
                title={"Login"}
                onCancel={hideModal}
                onOk={this.onLoginSubmitHandler}
                visible={visible}>
                <Form className="login-form">
                    <Form.Item>
                        {getFieldDecorator('email', {
                            rules: [{ required: true, message: 'Please input your email!' }],
                        })(
                            <Input
                                prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                                placeholder="Username"
                            />,
                        )}
                    </Form.Item>
                    <Form.Item>
                        {getFieldDecorator('password', {
                            rules: [{ required: true, message: 'Please input your Password!' }],
                        })(
                            <Input
                                prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                                type="password"
                                placeholder="Password"
                            />,
                        )}
                    </Form.Item>
                </Form>
            </Modal>

        );
    }
}

export default Form.create()(Login);