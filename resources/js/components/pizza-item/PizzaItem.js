import React, {useState} from 'react';
import {Card, Icon, Modal, Tooltip, Typography} from "antd";
import OrderModal from '../order/OrederModal';

const {Paragraph} = Typography;

const initialState = {isModalVisible: false};

const PizzaItem = ({pizza:{id, name, size, type}}) => {

    const [state, setState] = useState(initialState);

    const handleOrderNowClicked = () => {
        setState({isModalVisible: true});
    }

    const renderOrderNowModal = () => {
        return (
            <OrderModal
                pizzaName={name}
                pizzaId={id}
                isModalVisible={state.isModalVisible}
                closeModal={() => setState({isModalVisible: false})}/>
        )
    }

    return [
        <Card
            style={{width: '100%'}}
            actions={[
                <Tooltip title="Order it now!">
                    <Icon onClick={handleOrderNowClicked} type="plus-circle" key="order now"/>
                </Tooltip>
            ]}
        >
            <Typography>
                <Paragraph>
                    <h4><strong>Pizza</strong></h4>
                    <span>
                    {name}
                    </span>
                </Paragraph>
                <Paragraph>
                    <h4><strong>Size</strong></h4>
                    <span>
                    {size.name}
                    </span>
                </Paragraph>
                <Paragraph>
                    <h4><strong>Type</strong></h4>
                    <span>
                    {type.name}

                    </span>
                </Paragraph>
            </Typography>
        </Card>,
        renderOrderNowModal()
    ];
}


export default PizzaItem;