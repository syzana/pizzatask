import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import Pizza from "../containers/Pizza";
import 'antd/dist/antd.css';

export default class PizzaTask extends Component {
    render() {
        return (
            <Pizza/>
        );
    }
}

if (document.getElementById('pizzatask')) {
    ReactDOM.render(<PizzaTask />, document.getElementById('pizzatask'));
}
