class CookieUtil {

    static getAuthorizationReqConfig(){
        const jwtToken = CookieUtil.getCookie(JWT_TOKEN);
        return jwtToken ? {
            headers:{
                'Authorization': `Bearer ${jwtToken}`
            }
        } : null;
    }

    static getCookie(cname) {
        let name = cname + "=";
        let decodedCookie = decodeURIComponent(document.cookie);
        let ca = decodedCookie.split(';');
        for (let i = 0; i < ca.length; i++) {
            let c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    static deleteCookie(name) {
        document.cookie = name + '=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
    }


    static addOrReplaceCookie(name, token, path) {
        let cookieValue = name + '=' + token + ';';
        if (!Object.is(path, undefined) && typeof path === 'string' && path.length <= 50) {
            cookieValue = cookieValue + 'path=/' + path + ';'; //path doesn't exist at the moment, so cookies won't be sent to server
        }
        document.cookie = cookieValue + 'expires=Thu, 01 Jan 2999 00:00:01 GMT;'; //expires never
    }

    static addOrReplaceCookieWithExpiration(name, token, path, seconds) {
        let cookieValue = name + '=' + token + ';';
        if (!Object.is(path, undefined) && typeof path === 'string' && path.length <= 50) {
            cookieValue = cookieValue + 'path=/' + path + ';'; //path doesn't exist at the moment, so cookies won't be sent to server
        }
        let now = new Date();
        now.setTime(now.getTime() + (seconds * 1000));
        document.cookie = cookieValue + 'expires=' + now.toUTCString() + ';'; //expires never
    }
}

export const JWT_TOKEN = 'JWT_TOKEN';

export default CookieUtil;
