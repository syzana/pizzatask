<?php

namespace App\Http\Controllers;

use App\Order;
use App\PizzaOrder;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class OrderController extends Controller
{

    public function index(){
        return PizzaOrder::with(['order', 'pizza'])->get();
    }

    public function store(Request $request){

        $validatedData = $request->validate([
            'delivery_name' => 'required',
            'city' => 'required',
            'description' => 'required',
            'nr_of_pizzas' => 'required|min:1',
            'plz' => 'required',
            'street' => 'required',
            'pizza_id' => 'required'
        ]);


        $createdOrder = Order::create([
            'delivery_name' => $validatedData['delivery_name'],
            'city' => $validatedData['city'],
            'description' =>  $validatedData['description'],
            'plz' => $validatedData['plz'],
            'street' => $validatedData['street'],
        ]);

        PizzaOrder::create([
            'quantity' => $validatedData['nr_of_pizzas'],
            'pizza_id' => $validatedData['pizza_id'],
            'order_id' => $createdOrder->id
        ]);

        return response(['message' => 'Order is on the way!'], Response::HTTP_OK);
    }
}
