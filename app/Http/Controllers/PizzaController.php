<?php

namespace App\Http\Controllers;

use App\Pizza;
use Illuminate\Http\Request;

class PizzaController extends Controller
{

    public function index(){
        return Pizza::with(['size','type'])->get();
    }

    public function show($id){
        return Pizza::find($id);
    }
}
