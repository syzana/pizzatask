<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PizzaSize extends Model
{
    protected $table = 'pizza_size';

    public function pizza(){
        return $this->hasMany('\app\Pizza');
    }

}
