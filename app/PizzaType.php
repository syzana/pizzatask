<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PizzaType extends Model
{
    protected $table = 'pizza_type';

    public function pizza(){
        return $this->hasMany('\app\Pizza');
    }
}
