<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PizzaOrder extends Model
{
    protected $table = 'pizza_order';

    protected $fillable = ['quantity', 'pizza_id', 'order_id'];

    public function pizza(){
        return $this->belongsTo('App\Pizza', 'pizza_id');
    }

    public function order(){
        return $this->belongsTo('App\Pizza', 'order_id');
    }

}
