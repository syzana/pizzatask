<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pizza extends Model
{
    protected $hidden = ['pizza_size_id', 'pizza_type_id'];

    protected $table = 'pizza';

    public function size(){
        return $this->belongsTo('App\PizzaSize', 'pizza_size_id');
    }

    public function type(){
        return $this->belongsTo('App\PizzaType', 'pizza_type_id');
    }
}
