<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'order';

    protected $fillable = ['delivery_name', 'description', 'street', 'plz', 'city'];
}
