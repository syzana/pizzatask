<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PizzaOrder extends Migration
{

    const TABLE_NAME = 'pizza_order';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(PizzaOrder::TABLE_NAME, function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('quantity');
            $table->bigInteger('pizza_id')->unsigned();
            $table->foreign('pizza_id')->references('id')->on('pizza');
            $table->bigInteger('order_id')->unsigned();
            $table->foreign('order_id')->references('id')->on('order');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(PizzaOrder::TABLE_NAME, function (Blueprint $table) {
            $table->dropForeign('pizza_id');
            $table->dropForeign('order_id');
            $table->dropColumn('pizza_id');
            $table->dropColumn('order_id');
            Schema::drop(PizzaOrder::TABLE_NAME);
        });
    }
}
