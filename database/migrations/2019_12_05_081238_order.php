<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Order extends Migration
{
    const TABLE_NAME = 'order';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(Order::TABLE_NAME, function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('delivery_name');
            $table->string('description');
            $table->string('street');
            $table->string('plz');
            $table->string('city');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(Order::TABLE_NAME);
    }
}
