<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PizzaSize extends Migration
{
    const TABLE_NAME = 'pizza_size';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(PizzaSize::TABLE_NAME, function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->integer('size');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(PizzaSize::TABLE_NAME);
    }
}
