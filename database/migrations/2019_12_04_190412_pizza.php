<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Pizza extends Migration
{
    const TABLE_NAME = 'pizza';

    public function up()
    {
        Schema::create(Pizza::TABLE_NAME, function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->bigInteger('pizza_size_id')->unsigned();
            $table->foreign('pizza_size_id')->references('id')->on('pizza_size');
            $table->bigInteger('pizza_type_id')->unsigned();
            $table->foreign('pizza_type_id')->references('id')->on('pizza_type');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(Pizza::TABLE_NAME, function (Blueprint $table) {
            $table->dropForeign('pizza_size_id');
            $table->dropForeign('pizza_type_id');
            $table->dropColumn('pizza_size_id');
            $table->dropColumn('pizza_type_id');
            Schema::drop(Pizza::TABLE_NAME);
        });
    }
}