<?php

use Faker\Factory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PizzaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();
        for ($i = 0; $i < 4; $i++) {
            DB::table('pizza')->insert([
                'name' => $faker->sentence(),
                'pizza_size_id' => $faker->numberBetween(1,4),
                'pizza_type_id' => $faker->numberBetween(1,4),
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ]);
        }
    }
}
