<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use \Faker\Factory;

class PizzaSizeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();
        for ($i = 0; $i < 4; $i++) {
            DB::table('pizza_size')->insert([
                'name' => $faker->sentence(),
                'size' => $faker->numberBetween(20,60),
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ]);
        }
    }
}
