<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use \Faker\Factory;

class PizzaTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();
        for ($i = 0; $i < 4; $i++) {
            DB::table('pizza_type')->insert([
                'name' => $faker->sentence(),
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ]);
        }
    }
}
